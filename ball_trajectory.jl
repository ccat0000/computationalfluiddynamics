using Plots 


function analitical_solution(g, v0, ts, hs)
    @. hs = - 0.5 * g * ts ^2 + v0 * ts
end

function numerical_solution(g, v0, ts, hs)
    dt = ts[2] - ts[1]
    hs[1] = 0.0
    for i in 2:length(ts)
        hs[i] = hs[i-1] + (-g*ts[i-1] + v0) * dt
    end
end

function plot_trajectories(dt=0.01)
    g = 9.8
    v0 = 2.0
    ts = range(0, 2*v0/g, step=dt)
    hs = similar(ts)
    analitical_solution(g, v0, ts, hs)
    plot(ts, hs, label="Analitical")
    numerical_solution(g, v0, ts, hs)
    plot!(ts, hs, st=:scatter, label="Numerical")
end

plot_trajectories(0.02)
plot_trajectories(0.01)
plot_trajectories(0.005)